/* -----------------------------------------------------------------------------
 *
 *     Copyright (c) 2017  GEMALTO DEVELOPMENT - R&D
 *
 * ------------------------------------------------------------------------------
 * GEMALTO MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. GEMALTO SHALL NOT BE
 * LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING,
 * MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 * THIS SOFTWARE IS NOT DESIGNED OR INTENDED FOR USE OR RESALE AS ON-LINE
 * CONTROL EQUIPMENT IN HAZARDOUS ENVIRONMENTS REQUIRING FAIL-SAFE
 * PERFORMANCE, SUCH AS IN THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT
 * NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, DIRECT LIFE
 * SUPPORT MACHINES, OR WEAPONS SYSTEMS, IN WHICH THE FAILURE OF THE
 * SOFTWARE COULD LEAD DIRECTLY TO DEATH, PERSONAL INJURY, OR SEVERE
 * PHYSICAL OR ENVIRONMENTAL DAMAGE ("HIGH RISK ACTIVITIES"). GEMALTO
 * SPECIFICALLY DISCLAIMS ANY EXPRESS OR IMPLIED WARRANTY OF FITNESS FOR
 * HIGH RISK ACTIVITIES.
 *
 * ------------------------------------------------------------------------------
 */

#import "IDVZPlugin.h"
#import "FaceViewController.h"

@interface IDVZPlugin()

//@property(nonatomic, strong) EkycSDKIntegration* integrationEngine;

@end

@implementation IDVZPlugin

int txtStyle;
NSString *txtColor;
NSString *bgColor;
NSString *txtFamily;
NSString *fbBgColor;
double frameSize;
bool *setPreEnrollmentScreenEnabled;
NSString *appId;
NSString *expiryDate;
NSString *key;
NSString *appToken;
NSString *license;

#pragma mark public

- (void)initialize:(CDVInvokedUrlCommand *)command {
    
    appId = [command.arguments objectAtIndex:0];
    expiryDate = [command.arguments objectAtIndex:1];
    key = [command.arguments objectAtIndex:2];
    appToken = [command.arguments objectAtIndex:3];
    license =  [NSString stringWithFormat:@"appId=\"%@\"\nexpiryDate=%@\nkey=%@",appId, expiryDate, key];
    
    
    // Check command.arguments here.
    FaceViewController* nativeView = [[FaceViewController alloc] init];
    nativeView.callbackId = command.callbackId;
    nativeView.commandDelegate = self.commandDelegate;
    
    nativeView.license = license;
    nativeView.appToken = appToken;
    
    if(txtColor != nil){
        nativeView.txtColor = txtColor;
    }
    if(txtFamily != nil){
        nativeView.txtFamily = txtFamily;
        nativeView.txtStyle = txtStyle;
    }
    if(bgColor != nil){
        nativeView.bgColor = bgColor;
    }
    if(fbBgColor != nil){
        nativeView.fbBgColor = fbBgColor;
    }
    if(frameSize != 0){
        nativeView.frameSize = frameSize;
    }
    
    nativeView.setPreEnrollmentScreenEnabled = setPreEnrollmentScreenEnabled;
    [self.viewController presentViewController:nativeView animated:YES completion:nil];
}

- (void)setTextColor:(CDVInvokedUrlCommand*)command{
    txtColor = [command.arguments objectAtIndex:0];
}
- (void)setBackgroundColor:(CDVInvokedUrlCommand*)command{
    bgColor = [command.arguments objectAtIndex:0];
}
- (void)setTextType:(CDVInvokedUrlCommand*)command{
    txtFamily = [command.arguments objectAtIndex:0];
    txtStyle = [[command.arguments objectAtIndex:1] intValue];
}
- (void)setFeedbackBackgroundColor:(CDVInvokedUrlCommand*)command{
    fbBgColor = [command.arguments objectAtIndex:0];
}
- (void)setFrameSizeRatio:(CDVInvokedUrlCommand*)command{
    frameSize = [[command.arguments objectAtIndex:0] doubleValue];
}
- (void)setPreEnrollmentScreenEnabled:(CDVInvokedUrlCommand*)command{
    setPreEnrollmentScreenEnabled = [[command.arguments objectAtIndex:0] boolValue];
}
@end

