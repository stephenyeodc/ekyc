#import "FaceViewController.h"
#import <AVFoundation/AVFoundation.h>
@interface FaceViewController () <FaceCaptureDelegate>
@property (weak, nonatomic) IBOutlet UILabel *actionLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation FaceViewController

UIViewController *vc;

- (void)viewDidLoad {
    [super viewDidLoad];
    //    self.imageView.hidden = true;
    //    self.imageView.layer.cornerRadius = self.imageView.bounds.size.width / 2;
    self.actionLabel.numberOfLines = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    FaceCaptureCustomization *customization = [FaceCaptureCustomization sharedInstance];
    
    if(self.txtColor != nil)
        [customization setTextColor:[self getUIColorFromHexString:self.txtColor alpha:0.9]];
    
    if(self.bgColor != nil)
        [customization setBackgroundColor:[self getUIColorFromHexString:self.bgColor alpha:0.9]];
    
    if(self.txtFamily != nil)
        [customization setTextFont:[UIFont fontWithName:self.txtFamily size:self.txtStyle]];
    
    if(self.fbBgColor != nil)
        [customization setFeedbackBackgroundColor:[self getUIColorFromHexString:self.fbBgColor alpha:0.9]];
    
    if(self.frameSize != 0)
        [customization setFrameSizeRatio:self.frameSize];
    
    [customization setPreEnrollmentScreenEnabled:self.setPreEnrollmentScreenEnabled];
    
    
    [FaceCaptureSDK initializeWithLicenceText:self.license  appToken: self.appToken completion:^(BOOL successful, NSString *statusMessage) {
        if (successful) {
            
            vc = [FaceCaptureSDK createFaceCaptureVCWithDelegate:self];
            [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [self presentViewController:vc animated:NO completion:nil];
            
        } else {
            [self showErrorMessage:statusMessage];
        }
    }];
}

- (UIViewController*) topMostController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

- (UIColor *)getUIColorFromHexString:(NSString*)hexStr alpha:(CGFloat)alpha{
    unsigned int hexInt = [self intFromHexString:hexStr];
    
    UIColor *color = [UIColor colorWithRed:((CGFloat)((hexInt & 0xFF0000)>>16))/255 green:((CGFloat)((hexInt & 0xFF00)>>8))/255 blue:((CGFloat)(hexInt & 0xFF))/255 alpha:alpha];
    return color;
    
}

- (unsigned int)intFromHexString:(NSString*)hexStr{
    unsigned int hexInt = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexStr];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    [scanner scanHexInt:&hexInt];
    return hexInt;
}

-(void)showErrorMessage:(NSString *)message{
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@""
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self cancelAction:self];
        
    }];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:nil];
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return (UIInterfaceOrientationMaskPortrait);
}

- (BOOL)prefersStatusBarHidden {
    return false;
}

- (IBAction)cancelAction:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}


- (void)onFaceCaptureResult:(FaceCaptureResult *)result {
    
    if(result.verificationSucceed){
        
        
        NSString *faceMap = [result.faceMap base64EncodedStringWithOptions:0];
        NSString *faceImage = [result.image base64EncodedStringWithOptions:NSUTF8StringEncoding];
        NSString *sessionId = result.sessionId;
        
        id objects[] = {faceImage, faceMap, sessionId};
        id keys[] = {@"faceImage",@"faceMap",@"sessionId"};
        
        NSDictionary *dicResult = [[NSDictionary alloc] initWithObjects:objects forKeys:keys count:3];
        
        [self.commandDelegate runInBackground:^{
            CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:dicResult];
            [self.commandDelegate sendPluginResult:result callbackId:self.callbackId];
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:true completion:nil];
        });
        
    }else{
        [self.commandDelegate runInBackground:^{
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:result.livenessResult];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:self.callbackId];
        }];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:true completion:nil];
        });
    }
}

@end
