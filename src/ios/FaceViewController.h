//
//  FaceViewController.h
//  SampleApp
//
//  Created by Luciana Silva Daré on 28/03/19.
//

#import <UIKit/UIKit.h>
#import <IDV_Face_Z/IDV_Face_Z.h>
#import <Cordova/CDVPlugin.h>

NS_ASSUME_NONNULL_BEGIN

@interface FaceViewController : UIViewController
@property (nonatomic, strong) NSString* callbackId;
@property (nonatomic, weak) id <CDVCommandDelegate> commandDelegate;
@property int txtStyle;
@property (nonatomic, weak) NSString* txtColor;
@property (nonatomic, weak) NSString* bgColor;
@property (nonatomic, weak) NSString* txtFamily;
@property (nonatomic, weak) NSString* fbBgColor;
@property bool setPreEnrollmentScreenEnabled;
@property double frameSize;
@property (nonatomic, weak) NSString* license;
@property (nonatomic, weak) NSString* appToken;
@end


NS_ASSUME_NONNULL_END
