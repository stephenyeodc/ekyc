package cordova.plugin.idv_z;

import android.app.Activity;
import android.content.Context;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.util.Log;

import com.gemalto.ekyc.facez_capture.FaceCaptureActivity;

import org.apache.cordova.CallbackContext;

public class IDVZSDKIntegration {

    private static final String TAG = IDVZSDKIntegration.class.getSimpleName();

    private final Context mContext;

    public IDVZSDKIntegration(final Context context) {
        mContext = context;
    }

    public void setTextColor(@NonNull final String textColor){
        CaptureFaceActivity.txtColor = textColor;
    }

    public void setBackgroundColor(@NonNull final String bgColor){
        CaptureFaceActivity.bgColor = bgColor;
    }

    public void setTextType(@NonNull final String txtFamily, @NonNull final int txtStyle){
        CaptureFaceActivity.txtFamily = txtFamily;
        CaptureFaceActivity.txtStyle = txtStyle;

    }
    
    public void setFeedbackBackgroundColor(@NonNull final String bgColor){
        CaptureFaceActivity.fbBgColor = bgColor;
    }
    
    public void setFrameSizeRatio(@NonNull final double frameSize){
        CaptureFaceActivity.frameSize = frameSize;
    }
    
    public void setPreEnrollmentScreenEnabled(@NonNull final boolean isEnabled){
        CaptureFaceActivity.setPreEnrollmentScreenEnabled = isEnabled;
    }
    
    public void initialize(@NonNull final String appId, @NonNull final String expiryDate, @NonNull final String key, @NonNull final String appToken, @NonNull final CallbackContext callbackContext){
        if (mContext instanceof Activity) {
            CaptureFaceActivity.license = "appId=\""+ appId + "\"\nexpiryDate=" + expiryDate + "\nkey=" + key;
            CaptureFaceActivity.appToken = appToken;
            CaptureFaceActivity.initialize((Activity)mContext, callbackContext);
        }
    }
}
