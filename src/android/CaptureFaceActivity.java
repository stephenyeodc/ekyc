package cordova.plugin.idv_z;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;

import org.apache.cordova.CallbackContext;
import org.json.JSONException;
import org.json.JSONObject;
import com.gemalto.ekyc.facez_capture.FaceCaptureActivity;
import com.gemalto.ekyc.facez_capture.FaceCaptureCustomization;
import com.gemalto.ekyc.facez_capture.FaceCaptureResult;
import com.gemalto.ekyc.facez_capture.FaceCaptureSDK;
import com.gemalto.ekyc.facez_capture.FaceCaptureStatusManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CaptureFaceActivity extends AppCompatActivity
{
    public static String txtColor;
    public static String bgColor;
    public static String fbBgColor;
    public static String txtFamily;
    public static String license;
    public static String appToken;
    public static int txtStyle;
    public static double frameSize;
    public static boolean setPreEnrollmentScreenEnabled = false;

    //private FaceCaptureManager faceCaptureManager;
    static boolean active = false;

    private static CallbackContext callbackContext;

    public static void initialize(Activity activity, CallbackContext cContext) {//, String faceChoice){
        callbackContext = cContext;
        Intent intent = new Intent(activity, CaptureFaceActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startFaceCapture();
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    protected void onStop() {
        try {
            active = false;
        } catch (Exception ignore) {
        }
        super.onStop();
    }

    void startFaceCapture() {

        FaceCaptureCustomization cus = FaceCaptureCustomization.getInstance();
        if(txtColor != null){
            cus.setTextColor(txtColor);
        }
        if(bgColor != null){
            cus.setBackgroundColor(bgColor);
        }
        if(txtFamily != null){
            Typeface typeface = Typeface.create(txtFamily, txtStyle);
            cus.setTextFont(typeface);
        }
        if(fbBgColor != null){
            cus.setFeedbackBackgroundColor(fbBgColor);
        }
        if(frameSize != 0){
            cus.setFrameSizeRatio((float)frameSize);
        }
        if(setPreEnrollmentScreenEnabled){
            cus.setPreEnrollmentScreenEnabled(true);
        }

        FaceCaptureSDK.initialize(this, license, appToken, new FaceCaptureSDK.InitializeCallback() {
            @Override
            public void onCompletion(boolean successful, String message) {
                if(!successful) {
                    callbackContext.error(message);
                    finish();
                }
                Intent verificationIntent = new Intent(CaptureFaceActivity.this, FaceCaptureActivity.class);
                startActivityForResult(verificationIntent, FaceCaptureActivity.REQUEST_CODE_VERIFICATION);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == FaceCaptureStatusManager.RESULT_CODE_SUCCEED && requestCode == FaceCaptureActivity.REQUEST_CODE_VERIFICATION) {
            FaceCaptureResult result = data.getParcelableExtra(FaceCaptureActivity.PARCELABLE_RESULT);
            if(result == null) {
                callbackContext.error("Face Capture Failed!");
                finish();
            }

            String faceImage = android.util.Base64.encodeToString(result.getImage(), android.util.Base64.DEFAULT);
            String facemapStr = android.util.Base64.encodeToString(result.getFaceMap(), Base64.NO_WRAP);
            String sessionId = result.getSessionID();

            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("faceImage", faceImage);
                jsonObject.put("faceMap", facemapStr);
                jsonObject.put("sessionId", sessionId);
                callbackContext.success(jsonObject);
                finish();
            }catch (Exception e) {
                callbackContext.error(e.getMessage());
                finish();
            }

        }
        if(resultCode == FaceCaptureStatusManager.RESULT_CODE_FAILED) {
            FaceCaptureResult result = data.getParcelableExtra(FaceCaptureActivity.PARCELABLE_RESULT);
            callbackContext.error(result.getLivenessResult());
            finish();
        }
        if (resultCode == FaceCaptureStatusManager.RESULT_CODE_CANCELED) {
            FaceCaptureResult result = data.getParcelableExtra(FaceCaptureActivity.PARCELABLE_RESULT);
            callbackContext.error(result.getLivenessResult());
            finish();
        }
        return;
    }
}
