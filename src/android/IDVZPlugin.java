package cordova.plugin.idv_z;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

/**
 * IDVZ plugin wrapping the ekyc SDK.
 */
public class IDVZPlugin extends CordovaPlugin {

    private IDVZSDKIntegration idvzsdkIntegration;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void pluginInitialize() {
        super.pluginInitialize();

        idvzsdkIntegration = new IDVZSDKIntegration(cordova.getActivity());
    }

    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if (action.equals("setTextColor")) {
            final String txtColor = data.getString(0);
            idvzsdkIntegration.setTextColor(txtColor);
        } else if (action.equals("initialize")) {
            final String appId = data.getString(0);
            final String expiryDate = data.getString(1);
            final String key = data.getString(2);
            final String appToken = data.getString(3);
            idvzsdkIntegration.initialize(appId, expiryDate, key, appToken, callbackContext);
            return true;
        } else if (action.equals("setBackgroundColor")) {
            final String bgColor = data.getString(0);
            idvzsdkIntegration.setBackgroundColor(bgColor);
        } else if(action.equals("setTextType")) {
            final String txtFamily = data.getString(0);
            final int txtStyle = data.getInt(1);
            idvzsdkIntegration.setTextType(txtFamily,txtStyle);
        }else if (action.equals("setFeedbackBackgroundColor")) {
            final String bgColor = data.getString(0);
            idvzsdkIntegration.setFeedbackBackgroundColor(bgColor);
        }else if(action.equals("setFrameSizeRatio")) {
            final double frameSize = data.getDouble(0);
            idvzsdkIntegration.setFrameSizeRatio(frameSize);
        }else if(action.equals("setPreEnrollmentScreenEnabled")) {
            final boolean setPreEnrollmentScreenEnabled = data.getBoolean(0);
            idvzsdkIntegration.setPreEnrollmentScreenEnabled(setPreEnrollmentScreenEnabled);
        }
        return false;
    }
}
