var exec = require('cordova/exec');

var PLUGIN_NAME = 'IDVZPlugin';

function IDVZPlugin() {
}

IDVZPlugin.prototype.initialize = function(appId, expiryDate, key, appToken, callback, errorCallback) {
    exec(callback, errorCallback, PLUGIN_NAME, 'initialize', [appId, expiryDate, key, appToken]);
};

IDVZPlugin.prototype.setTextColor = function(color, callback, errorCallback) {
    exec(callback, errorCallback, PLUGIN_NAME, 'setTextColor', [color]);
};

IDVZPlugin.prototype.setBackgroundColor = function(color, callback, errorCallback) {
    exec(callback, errorCallback, PLUGIN_NAME, 'setBackgroundColor', [color]);
};

IDVZPlugin.prototype.setTextType = function(family,style, callback, errorCallback) {
    exec(callback, errorCallback, PLUGIN_NAME, 'setTextType', [family,style]);
};

IDVZPlugin.prototype.setFeedbackBackgroundColor = function(color, callback, errorCallback) {
    exec(callback, errorCallback, PLUGIN_NAME, 'setFeedbackBackgroundColor', [color]);
};

IDVZPlugin.prototype.setFrameSizeRatio = function(frameSize, callback, errorCallback) {
    exec(callback, errorCallback, PLUGIN_NAME, 'setFrameSizeRatio', [frameSize]);
};

IDVZPlugin.prototype.setPreEnrollmentScreenEnabled = function(setPreEnrollmentScreenEnabled, callback, errorCallback) {
    exec(callback, errorCallback, PLUGIN_NAME, 'setPreEnrollmentScreenEnabled', [setPreEnrollmentScreenEnabled]);
};

var idvzPlugin = new IDVZPlugin();
module.exports = idvzPlugin;
