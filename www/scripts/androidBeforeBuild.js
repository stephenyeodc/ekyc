var fsCallback = function (err) {
    if (err) {
        console.error("Failed to create directory or file.");
        throw err;
    }
}
module.exports = function(context) {
    var fs = require('fs');
	var fsEx = require("fs.extra");

    console.log("Before build js for Android");

    // Copy CaptureFaceActivity.java
    fsEx.copy('plugins/cordova.plugin.idv_z/src/android/CaptureFaceActivity.java', 'platforms/android/app/src/main/java/cordova/plugin/idv_z/CaptureFaceActivity.java', { replace: true }, fsCallback);

    // Copy idv_face-z.license to app/src/main/assets
    fsEx.copy('plugins/cordova.plugin.idv_z/src/android/idv_face-z.license', 'platforms/android/app/src/main/assets/idv_face-z.license', { replace: true }, fsCallback);
    
    // Copy License
    fsEx.copy('plugins/cordova.plugin.idv_z/src/android/License.java', 'platforms/android/app/src/main/java/cordova/plugin/idv_z/License.java', { replace: true }, fsCallback);
    
    // Copy activity_capture_doc.xml to res/layout
    fsEx.mkdirp('platforms/android/app/src/main/res/values', fsCallback);
    fsEx.copy('plugins/cordova.plugin.idv_z/src/res/values/zoom.xml', 'platforms/android/app/src/main/res/values/zoom.xml', { replace: false }, fsCallback);

    fsEx.mkdirp('platforms/android/app/src/main/res/layout', fsCallback);
    fsEx.copy('plugins/cordova.plugin.idv_z/src/res/layout/activity_capture_face.xml', 'platforms/android/app/src/main/res/layout/activity_capture_face.xml', { replace: true }, fsCallback);
    
    console.log('Customization done for Android');

};
